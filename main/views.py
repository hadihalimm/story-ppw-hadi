from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import MataKuliah, Kegiatan, Person
from main.forms import MatkulForm, KegiatanForm, PersonForm, UserForm
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
import requests
import json


def home(request):
    return render(request, 'main/home.html')

def aboutme(request):
    matkul = MataKuliah.objects.all()
    response = {}
    response['matkul'] = matkul
    return render(request, 'main/aboutme.html',response)

def resume(request):
    return render(request, 'main/resume.html')

def projects(request):
    return render(request, 'main/projects.html')

def addmatkul(request):
    form = MatkulForm()
    if request.method == 'POST' and form.is_valid:
        form = MatkulForm(request.POST)
        form.save()
        return redirect('/')
    context = {'form':form}

    return render(request, 'main/addmatkul.html', context)

def deletematkul(request, pk):
    matkul = MataKuliah.objects.get(kode_matkul=pk)
    if request.method == 'POST':
        matkul.delete()
        return redirect('/aboutme')

    context ={'item':matkul}
    return render(request, 'main/deletematkul.html', context)

def kegiatan(request):
    kegiatans = Kegiatan.objects.all()
    persons = Person.objects.all()
    response = {}
    response['kegiatans'] = kegiatans
    response['persons'] = persons

    return render(request, 'main/kegiatan.html', response)

def addkegiatan(request):
    form = KegiatanForm()
    if request.method == 'POST' and form.is_valid:
        form = KegiatanForm(request.POST)
        form.save()
        return redirect('/kegiatan')
    context = {'form':form}

    return render(request, 'main/addkegiatan.html', context)

def addperson(request):
    #kegiatan_temp = Kegiatan.objects.get(kode_kegiatan = pk)
    form = PersonForm()
    if request.method == 'POST' and form.is_valid:
        form = PersonForm(request.POST)
        form.save()
        return redirect('/kegiatan')
    context = {'form':form}

    return render(request, 'main/addperson.html', context)

def books(request):

    if request.method == 'GET':
        judul = request.GET.get('judul')
        url = 'https://www.googleapis.com/books/v1/volumes?q=intitle:{}'
        response = requests.get(url.format(judul)).json()
        context = {'data': response['items']}

        print(context['data'])

    return render(request, 'main/books.html', context)

def login(request):

    username = ""
    password = ""
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

    user = authenticate(request, username=username, password=password)
    if user is not None:
        auth_login(request, user)
        return redirect('/')

    return render(request, 'main/login.html')

def register(request):
    form = UserForm()
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login')

    return render(request, 'main/register.html', {'form':form})

def logout(request):
    auth_logout(request)
    return redirect('/login')