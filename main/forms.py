from django import forms
from django.forms import ModelForm
from .models import MataKuliah
from .models import Kegiatan, Person
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class MatkulForm(ModelForm):
    class Meta:
        model = MataKuliah
        fields = ['nama_matkul','kode_matkul','dosen','jumlah_sks','deskripsi','semester_tahun','ruang_kelas']

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama_kegiatan', 'kode_kegiatan', 'deskripsi_kegiatan']

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = ['nama', 'id_person', 'kegiatan']

class UserForm(UserCreationForm):
    fname = forms.CharField()
    lname = forms.CharField()

    class Meta:
        model = User
        fields = ['fname', 'lname', 'username', 'password1', 'password2']