from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('resume/', views.resume, name='resume'),
    path('projects/', views.projects, name='projects'),
    path('addmatkul/', views.addmatkul, name='addmatkul'),
    path('deletematkul/<str:pk>/',views.deletematkul, name='deletematkul'),
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('addkegiatan/', views.addkegiatan, name='addkegiatan'),
    path('addperson/', views.addperson, name='addperson'),
    path('books/', views.books, name='books'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout, name='logout'),
]
