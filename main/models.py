from django.db import models

# Create your models here.

class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=30)
    kode_matkul = models.CharField(max_length=10)
    dosen = models.CharField(max_length=30)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_matkul

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    kode_kegiatan = models.CharField(max_length=5)
    deskripsi_kegiatan = models.TextField()

    def __str__(self):
        return self.nama_kegiatan

class Person(models.Model):
    nama = models.CharField(max_length=30)
    id_person = models.CharField(max_length=5)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama