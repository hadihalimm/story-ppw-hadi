from django.contrib import admin
from .models import MataKuliah, Kegiatan, Person

# Register your models here.
admin.site.register(MataKuliah)
admin.site.register(Kegiatan)
admin.site.register(Person)